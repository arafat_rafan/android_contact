/*
 * Project Name: libsbsipsdk [SB SIP PHONE SDK]
 * Developed By: Brotecs Technologies Limited
 * File Name: ModelSetterName.java
 * Created By: Mohammded Yeasin Arafat
 * Created Date: 1/19/16, 1:25 PM
 * Updated By: Mohammded Yeasin Arafat
 * Updated Date:  1/19/16
 */
package com.sb.sdk.contactmanager;

/**
 * <h1>ModelSetterName</h1>
 * <p>
 * ModelSetterName is an enum. It declares all possible setter method name
 * </p>
 *
 * @author Brotecs Technologies Limited
 * @version 1.0.1-26012016
 * @since 1/19/16
 */
public enum ModelSetterName {
    setId,setDisplayName,setImageUri,setDefaultNumberAndType,setNumberAndType, setEmailAndType,setPostalAddressAndType
}
