/*
 * Project Name: libsbsipsdk [SB SIP PHONE SDK]
 * Developed By: Brotecs Technologies Limited
 * File Name: Softphonecontact.java
 * Created By: Mohammded Yeasin Arafat
 * Created Date: 1/19/16, 1:19 PM
 * Updated By: Mohammded Yeasin Arafat
 * Updated Date:  1/19/16
 */
package com.sb.sdk.contactmanager;

import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>ISoftPhoneContactModel</h1>
 * <p>
 * ISoftPhoneContactModel is derived from ISoftPhoneContact enlist all the possible setter method of SoftPhoneContact
 * </p>
 *
 * @author Brotecs Technologies Limited
 * @version 1.0.1-26012016
 * @since 1/19/16
 */
public interface ISoftPhoneContactModel extends ISoftPhoneContact{
    /**
     * Responsible to set Contact ID
     * @param id
     */
    public void setId(String id);
    /**
     * Responsible to set Display Name
     * @param displayName
     */
    public void setDisplayName(String displayName);
    /**
     * Responsible to set Image Uri
     * @param imageUri
     */
    public void setImageUri(Uri imageUri);
    /**
     * Responsible to set {@link PhoneNumberAndType} - Default Phone Number And Type
     * @param defaultNumberAndType
     */
    public void setDefaultNumberAndType(PhoneNumberAndType defaultNumberAndType);
    /**
     * Responsible to set {@link List <PhoneNumberAndType>} - list of Phone Number And Type
     * @param numberAndType
     */
    public void setNumberAndType(ArrayList<PhoneNumberAndType> numberAndType);
    /**
     * Responsible to set {@link List <EmailAndType>} - list of Email And Type
     * @param emailAddressAndType
     */
    public void setEmailAndType(ArrayList<EmailAndType> emailAddressAndType);
    /**
     * Responsible to set {@link List <PostalAddressAndType>} - list of Postal Address And Type
     * @param postalAddressAndType
     */
    public void setPostalAddressAndType(ArrayList<PostalAddressAndType> postalAddressAndType);

}
