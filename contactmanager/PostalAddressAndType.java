/*
 * Project Name: libsbsipsdk [SB SIP PHONE SDK]
 * Developed By: Brotecs Technologies Limited
 * File Name: PostalAddressAndType.java
 * Created By: Mohammded Yeasin Arafat
 * Created Date: 1/19/16, 1:26 PM
 * Updated By: Mohammded Yeasin Arafat
 * Updated Date:  1/19/16
 */
package com.sb.sdk.contactmanager;

/**
 * <h1>PostalAddressAndType</h1>
 * <p>
 * PostalAddressAndType is model class which represent of contact's single Postal Address And Type
 * </p>
 *
 * @author Brotecs Technologies Limited
 * @version 1.0.1-26012016
 * @since 1/19/16
 */
public class PostalAddressAndType {
    /**
     * "address" represents Single Postal Address
     */
    private String address;
    /**
     * "addressType" represents Single Postal Address's type
     */
    private String addressType;

    /**
     *
     * @param address
     * @param addressType
     */
    public PostalAddressAndType(String address, String addressType){
        this.address = address;
        this.addressType = addressType;
    }

    /**
     * Responsible to return single Postal Address
     * @return {@link String} address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Responsible to return single Postal Address
     * @return {@link String} addressType
     */
    public String getAddressType() {
        return addressType;
    }

    @Override
    public String toString() {
        return "PostalAddressAndType{" +
                "address='" + address + '\'' +
                ", addressType='" + addressType + '\'' +
                '}';
    }
}