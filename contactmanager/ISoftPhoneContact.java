/*
 * Project Name: libsbsipsdk [SB SIP PHONE SDK]
 * Developed By: Brotecs Technologies Limited
 * File Name: ISoftPhoneContact.java
 * Created By: Mohammded Yeasin Arafat
 * Created Date: 1/21/16, 12:54 PM
 * Updated By: Mohammded Yeasin Arafat
 * Updated Date:  1/21/16
 */
package com.sb.sdk.contactmanager;

/**
 * <h1>ISoftPhoneContact</h1>
 * <p>
 * ISoftPhoneContact should parent class of any Model/bean class of Contact Data set Model
 * To pass any Model/Bean to ContactInfo it must implement ISoftPhoneContact or extend/implement any of it's derived class/Interface
 * </p>
 *
 * @author Brotecs Technologies Limited
 * @version 1.0.1-26012016
 * @since 1/21/16
 */
public interface ISoftPhoneContact {
}