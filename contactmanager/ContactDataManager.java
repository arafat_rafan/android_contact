/*
 * Project Name: libsbsipsdk [SB SIP PHONE SDK]
 * Developed By: Brotecs Technologies Limited
 * File Name: CommonDataManager.java
 * Created By: Mohammded Yeasin Arafat
 * Created Date: 1/19/16, 2:55 PM
 * Updated By: Mohammded Yeasin Arafat
 * Updated Date:  1/19/16
 */
package com.sb.sdk.contactmanager;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * <h1>ContactDataManager</h1>
 * <p>
 * ContactDataManager is responsible to fetch all Contact Data from Phone and manipulate Data set according to available Data
 * </p>
 *
 * @author Brotecs Technologies Limited
 * @version 1.0.1-26012016
 * @since 1/19/16
 */
public class ContactDataManager<T extends ISoftPhoneContact> {
    /**
     * "contentResolver" instance of {@link ContentResolver}
     */
    ContentResolver contentResolver = null;
    /**
     * "context" instance of {@link Context}
     */
    Context context = null;
    /**
     * "dynamicContactModel" instance of T which is Custom Model/Bean and must ISoftPhoneContact class or child class of ISoftPhoneContact type
     */
    T dynamicContactModel;
    /**
     * "selection" represents Selection parameter to fetch Contact Data from Native?Phone Contact
     */
    private String selection = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
    /**
     * "selectArgs" represents Selection parameter's value to fetch Contact Data from Native?Phone Contact
     */
    private String[] selectArgs = null;
    /**
     * "uri" instance of contentProvider {@link android.content.ContentProvider} to fetch Contact Data from Different ContentProvider
     */
    private Uri uri = null;
    /**
     * "projection" represents Type of Data to fetch from Different ContentProvider
     */
    private String[] projection = null;

    /**
     * Responsible to set ContentResolver, ISoftPhoneContact and Context
     * @param contentResolver {@link ContentResolver}
     * @param dynamicContactModel {@link T}
     * @param context {@link Context}
     */
    public ContactDataManager(ContentResolver contentResolver, T dynamicContactModel, Context context) {
        this.contentResolver = contentResolver;
        this.context = context;
        this.dynamicContactModel = dynamicContactModel;
    }

    /**
     * Responsible to fetch contact Data from phone
     * @param uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     * @return contactProviderCursor - {@link Cursor}
     */
    private Cursor getQueryResult(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor contactProviderCursor = null;
        if (contentResolver != null) {
            contactProviderCursor = contentResolver.query(
                    uri, projection,
                    selection, selectionArgs, sortOrder);
        }
        return contactProviderCursor;
    }

    /**
     * Responsible to return Contact ID
     * @param number
     * @return contactId - {@link String}
     */
    public String getContactID(String number) {
        String contactId = null;
        uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));
        projection = new String[]{ContactsContract.PhoneLookup._ID};

        Cursor contactProviderCursor = getQueryResult(uri, projection, null, null, null);
        if (contactProviderCursor != null) {

            if (contactProviderCursor.moveToNext()) {
                contactId = contactProviderCursor.getString(0);
            }
            contactProviderCursor.close();
        }
        if (this.dynamicContactModel != null) {
            setValueOfDynamicObject(this.dynamicContactModel, contactId, "setId");
        }
        return contactId;
    }

    /**
     * Responsible to insert DisplayName,Image Uri into Data set
     * @param id
     * @return dynamicContactModel - {@link T}
     */
    public T getSingleContactDisplayNameImageUriByID(String id, ArrayList<String> setterList) {
        uri = ContactsContract.Contacts.CONTENT_URI;
        projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME,ContactsContract.Contacts.PHOTO_URI};
        selection = ContactsContract.Contacts._ID;
        selectArgs = new String[]{id};

        Cursor contactProviderCursor = getQueryResult(uri, projection, selection + " = ?", selectArgs, null);
        if (contactProviderCursor != null) {
            if (contactProviderCursor.moveToNext()) {
                String displayName = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String imageUriString = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.Contacts.PHOTO_URI));
                Uri imageUri = null;
                if(imageUriString != null){
                    imageUri = Uri.parse(imageUriString);
                }
                if (setterList != null && setterList.size() > 0) {
                    for (String setterName : setterList
                            ) {
                        if (setterName.equals(ModelSetterName.setDisplayName.toString())) {
                            setValueOfDynamicObject(this.dynamicContactModel, displayName, setterName);
                        } else if (setterName.equals(ModelSetterName.setImageUri.toString())) {
                            setValueOfDynamicObject(this.dynamicContactModel, imageUri, setterName);
                        }
                    }
                } else {
                    setValueOfDynamicObject(this.dynamicContactModel, displayName, "setDisplayName");
                    setValueOfDynamicObject(this.dynamicContactModel, imageUri, "setImageUri");
                }
            }
            contactProviderCursor.close();
        }
        return dynamicContactModel;
    }

    /**
     * Responsible to insert DisplayName, Number And Type And Image Uri into Data set
     * @param id
     * @return dynamicContactModel - {@link T}
     */
    public T getSingleContactDisplayNameDefaultNumberAndTypeImageUriByID(String id, ArrayList<String> setterList) {
        uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.LABEL, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.PHOTO_URI};
        selectArgs = new String[]{id};
        Cursor contactProviderCursor = getQueryResult(uri, projection, selection + " = ?", selectArgs, null);
        if (contactProviderCursor != null) {
            if (contactProviderCursor.moveToNext()) {
                String number = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String numberLevel = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));

                if (numberLevel != null) {
                    if (context != null) {
                        numberLevel = ContactsContract.CommonDataKinds.Phone
                                .getTypeLabel(context.getResources(),
                                        Integer.parseInt(numberLevel), "")
                                .toString();
                        if (numberLevel.equalsIgnoreCase("custom")) {
                            numberLevel = contactProviderCursor
                                    .getString(
                                            contactProviderCursor
                                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL));
                        }
                    }
                }
                String displayName = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String imageUriString = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                Uri imageUri = null;
                if(imageUriString != null){
                    imageUri = Uri.parse(imageUriString);
                }
                PhoneNumberAndType phoneNumberAndType = new PhoneNumberAndType(number, numberLevel);
                if (setterList != null && setterList.size() > 0) {
                    for (String setterName : setterList
                            ) {
                        if (setterName.equals(ModelSetterName.setId.toString())) {
                            setValueOfDynamicObject(this.dynamicContactModel, id, setterName);
                        } else if (setterName.equals(ModelSetterName.setDisplayName.toString())) {
                            setValueOfDynamicObject(this.dynamicContactModel, displayName, setterName);
                        } else if (setterName.equals(ModelSetterName.setDefaultNumberAndType.toString())) {
                            setValueOfDynamicObject(this.dynamicContactModel, phoneNumberAndType, setterName);
                        } else if (setterName.equals(ModelSetterName.setImageUri.toString())) {
                            setValueOfDynamicObject(this.dynamicContactModel, imageUri, setterName);
                        }
                    }
                } else {
                    setValueOfDynamicObject(this.dynamicContactModel, id, "setId");
                    setValueOfDynamicObject(this.dynamicContactModel, displayName, "setDisplayName");
                    setValueOfDynamicObject(this.dynamicContactModel, imageUri, "setImageUri");
                    setValueOfDynamicObject(this.dynamicContactModel, phoneNumberAndType, "setDefaultNumberAndType");
                }
            }
            contactProviderCursor.close();
        }
        return dynamicContactModel;
    }

    /**
     * Responsible to insert DisplayName, Number And Type And Image Uri into Data set of All Contact
     * @param setterList List of setter method consist Dynamic Contact Model
     * @return allContactInfoList - {@link T} list of dynamicContactModel
     */
    @Deprecated
    public List<T> getContactDisplayNameDefaultNumberAndTypeImageUri(ArrayList<String> setterList) {
        boolean doSort = isComparableImplement(dynamicContactModel);
        uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.LABEL, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.PHOTO_URI};
        List<T> allContactInfoList = null;
        Set<T> tSet = new TreeSet<>();
        Cursor contactProviderCursor = getQueryResult(uri, projection, null, null, null);
        if (contactProviderCursor != null) {
            allContactInfoList = new ArrayList<>();
            while (contactProviderCursor.moveToNext()) {
                String number = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String numberLevel = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));

                if (numberLevel != null) {
                    if (context != null) {
                        numberLevel = ContactsContract.CommonDataKinds.Phone
                                .getTypeLabel(context.getResources(),
                                        Integer.parseInt(numberLevel), "")
                                .toString();
                        if (numberLevel.equalsIgnoreCase("custom")) {
                            numberLevel = contactProviderCursor
                                    .getString(
                                            contactProviderCursor
                                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL));
                        }
                    }
                }
                String id = getContactID(number);
                String displayName = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String imageUriString = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                Uri imageUri = null;
                if(imageUriString != null){
                    imageUri = Uri.parse(imageUriString);
                }
                PhoneNumberAndType phoneNumberAndType = new PhoneNumberAndType(number, numberLevel);
                Object newDynamicObject = getNewInstance(this.dynamicContactModel);

                if (setterList != null && setterList.size() > 0) {
                    for (String setterName : setterList
                            ) {
                        if (setterName.equals(ModelSetterName.setId.toString())) {
                            setValueOfDynamicObject((T) newDynamicObject, id, setterName);
                        } else if (setterName.equals(ModelSetterName.setDisplayName.toString())) {
                            setValueOfDynamicObject((T) newDynamicObject, displayName, setterName);
                        } else if (setterName.equals(ModelSetterName.setDefaultNumberAndType.toString())) {
                            setValueOfDynamicObject((T) newDynamicObject, phoneNumberAndType, setterName);
                        } else if (setterName.equals(ModelSetterName.setImageUri.toString())) {
                            setValueOfDynamicObject((T) newDynamicObject, imageUri, setterName);
                        }
                    }
                }else{
                    setValueOfDynamicObject((T)newDynamicObject, id, "setId");
                    setValueOfDynamicObject((T)newDynamicObject, displayName, "setDisplayName");
                    setValueOfDynamicObject((T)newDynamicObject, imageUri, "setImageUri");
                    setValueOfDynamicObject((T) newDynamicObject, phoneNumberAndType, "setDefaultNumberAndType");
                }
                if(doSort){
                    tSet.add((T) newDynamicObject);
                }else{
                    allContactInfoList.add((T) newDynamicObject);
                }
            }
            contactProviderCursor.close();
        }
        if(doSort){
            allContactInfoList = new ArrayList<>(tSet);
        }
        return allContactInfoList;
    }

    /**
     * Responsible to insert DisplayName, ID And Image Uri into Data set of All Contact
     * @param filterText - If null/Empty String then fetch all contact otherwise fetch only those contact where match with any data of contact
     * @param setterList List of setter method consist Dynamic Contact Model
     * @return allContactInfoList - {@link T} list of dynamicContactModel
     */
    public List<T> getContactDisplayNameIDImageUri(ArrayList<String> setterList, String filterText) {
        boolean doSort = isComparableImplement(dynamicContactModel);
        if(filterText != null && filterText.length() > 0){
            uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_FILTER_URI, Uri.encode(filterText));
        }else{
            uri = ContactsContract.Contacts.CONTENT_URI;
        }
        projection = new String[]{ ContactsContract.Profile._ID, ContactsContract.Profile.DISPLAY_NAME_PRIMARY, ContactsContract.Profile.PHOTO_URI};
        List<T> allContactInfoList = null;
        Set<T> tSet = new TreeSet<>();
        Cursor contactProviderCursor = getQueryResult(uri, projection, null, null, null);
        if (contactProviderCursor != null) {
            allContactInfoList = new ArrayList<>();
            while (contactProviderCursor.moveToNext()) {
                String id = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.Profile._ID));
                String displayName = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.Profile.DISPLAY_NAME_PRIMARY));
                String imageUriString = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.Profile.PHOTO_URI));
                Uri imageUri = null;
                if(imageUriString != null){
                    imageUri = Uri.parse(imageUriString);
                }
                Object newDynamicObject = getNewInstance(this.dynamicContactModel);

                if (setterList != null && setterList.size() > 0) {
                    for (String setterName : setterList
                            ) {
                        if (setterName.equals(ModelSetterName.setId.toString())) {
                            setValueOfDynamicObject((T) newDynamicObject, id, setterName);
                        } else if (setterName.equals(ModelSetterName.setDisplayName.toString())) {
                            setValueOfDynamicObject((T) newDynamicObject, displayName, setterName);
                        } else if (setterName.equals(ModelSetterName.setImageUri.toString())) {
                            setValueOfDynamicObject((T) newDynamicObject, imageUri, setterName);
                        }
                    }
                }else{
                    setValueOfDynamicObject((T)newDynamicObject, id, "setId");
                    setValueOfDynamicObject((T)newDynamicObject, displayName, "setDisplayName");
                    setValueOfDynamicObject((T)newDynamicObject, imageUri, "setImageUri");
                }
                if(displayName != null){
                    if(doSort){
                        tSet.add((T) newDynamicObject);
                    }else{
                        allContactInfoList.add((T) newDynamicObject);
                    }
                }
            }
            contactProviderCursor.close();
        }
        if(doSort){
            allContactInfoList = new ArrayList<>(tSet);
        }
        Log.i("ContactList"," allContactInfoList: "+allContactInfoList.size());
        return allContactInfoList;
    }

    /**
     * Responsible to insert list of Number And Type into Data set- {@link PhoneNumberAndType}
     * @param id
     * @return dynamicContactModel - {@link T}
     */
    public T getContactNumberAndTypeListByID(String id) {
        uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.LABEL};
        selectArgs = new String[]{id};
        selection = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;

        Cursor contactProviderCursor = getQueryResult(uri, projection, selection + " = ?", selectArgs, null);
        if (contactProviderCursor != null) {
            ArrayList<PhoneNumberAndType> phoneNumberAndTypes = new ArrayList<>();
            while (contactProviderCursor.moveToNext()) {
                String number = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String numberLevel = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));

                if (numberLevel != null) {
                    if (context != null) {
                        numberLevel = ContactsContract.CommonDataKinds.Phone
                                .getTypeLabel(context.getResources(),
                                        Integer.parseInt(numberLevel), "")
                                .toString();
                        if (numberLevel.equalsIgnoreCase("custom")) {
                            numberLevel = contactProviderCursor
                                    .getString(
                                            contactProviderCursor
                                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL));
                        }
                    }
                }

                PhoneNumberAndType phoneNumberAndType = new PhoneNumberAndType(number, numberLevel);
                phoneNumberAndTypes.add(phoneNumberAndType);
            }
            contactProviderCursor.close();
            if (phoneNumberAndTypes.size() > 0) {
                setValueOfDynamicObject(this.dynamicContactModel, phoneNumberAndTypes, "setNumberAndType");
            }
        }
        return dynamicContactModel;
    }

    /**
     * Responsible to insert list of Email And Type into Data set- {@link EmailAndType}
     * @param id
     * @return dynamicContactModel - {@link T}
     */
    public T getContactEmailAndTypeListByID(String id) {
        uri = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        projection = new String[]{ContactsContract.CommonDataKinds.Email.DATA, ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.LABEL};
        selectArgs = new String[]{id};
        selection = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;

        Cursor contactProviderCursor = getQueryResult(uri, projection, selection + " = ?", selectArgs, null);
        if (contactProviderCursor != null) {
            ArrayList<EmailAndType> emailAndTypes = new ArrayList<>();
            while (contactProviderCursor.moveToNext()) {
                String emailData = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                String emailType = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                emailType = ContactsContract.CommonDataKinds.Email
                        .getTypeLabel(context.getResources(),
                                Integer.parseInt(emailType), "").toString();
                if (emailType.equalsIgnoreCase("custom")) {
                    emailType = contactProviderCursor
                            .getString(
                                    contactProviderCursor
                                            .getColumnIndex(ContactsContract.CommonDataKinds.Email.LABEL));
                }

                EmailAndType emailAndType = new EmailAndType(emailData, emailType);
                emailAndTypes.add(emailAndType);
            }
            contactProviderCursor.close();
            if (emailAndTypes.size() > 0) {
                setValueOfDynamicObject(this.dynamicContactModel, emailAndTypes, "setEmailAndType");
            }
        }
        return dynamicContactModel;
    }

    /**
     * Responsible to insert list of Postal Address And Type into Data set- {@link PostalAddressAndType}
     * @param id
     * @return dynamicContactModel - {@link T}
     */
    public T getContactAddressAndTypeListByID(String id) {
        uri = ContactsContract.Data.CONTENT_URI;
        selection = ContactsContract.Data.CONTACT_ID + " = ? AND "
                + ContactsContract.Data.MIMETYPE + " = ?";
        selectArgs = new String[]{id, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE};
        Cursor contactProviderCursor = getQueryResult(uri, null, selection, selectArgs, null);
        if (contactProviderCursor != null) {
            ArrayList<PostalAddressAndType> postalAddressAndTypes = new ArrayList<>();
            while (contactProviderCursor.moveToNext()) {
                String poBox = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POBOX));
                String country = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
                String neighbourhood = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.NEIGHBORHOOD));
                String street = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                String city = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
                String state = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.REGION));
                String zipCode = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
                String type = contactProviderCursor
                        .getString(contactProviderCursor
                                .getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.TYPE));

                if (type != null) {
                    type = ContactsContract.CommonDataKinds.StructuredPostal
                            .getTypeLabel(context.getResources(),
                                    Integer.parseInt(type), "").toString();
                    if (type.equalsIgnoreCase("custom")) {
                        type = contactProviderCursor
                                .getString(
                                        contactProviderCursor
                                                .getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.LABEL));
                    }
                }
                String fullAddress = "";
                if (street != null) {
                    if (!street.isEmpty()) {
                        fullAddress += street + "<br/>";
                    }

                }
                if (poBox != null) {
                    if (!poBox.isEmpty()) {
                        fullAddress += poBox + "<br/>";
                    }

                }

                if (neighbourhood != null) {
                    if (!neighbourhood.isEmpty()) {
                        fullAddress += neighbourhood + "<br/>";
                    }

                }

                if (city != null && !city.isEmpty()) {

                    fullAddress += city;

                    if (state != null && !state.isEmpty()) {
                        fullAddress += ", " + state;

                        if (zipCode != null && !zipCode.isEmpty()) {
                            fullAddress += " " + zipCode + "<br/>";
                        } else {
                            fullAddress += "<br/>";
                        }

                    } else {
                        if (zipCode != null && !zipCode.isEmpty()) {
                            fullAddress += " " + zipCode + "<br/>";
                        } else {
                            fullAddress += "<br/>";
                        }
                    }


                } else if (state != null && !state.isEmpty()) {

                    fullAddress += state;
                    if (zipCode != null && !zipCode.isEmpty()) {
                        fullAddress += " " + zipCode + "<br/>";

                    } else {
                        fullAddress += "<br/>";
                    }


                } else if (zipCode != null && !zipCode.isEmpty()) {
                    fullAddress += zipCode + "<br/>";

                }

                if (country != null && !country.isEmpty()) {
                    fullAddress += country + "<br/>";

                }


                PostalAddressAndType postalAddressAndType = new PostalAddressAndType(fullAddress, type);
                postalAddressAndTypes.add(postalAddressAndType);
            }
            contactProviderCursor.close();
            if (postalAddressAndTypes.size() > 0) {
                setValueOfDynamicObject(this.dynamicContactModel, postalAddressAndTypes, "setPostalAddressAndType");
            }
        }
        return dynamicContactModel;
    }

    /**
     * Responsible to set value into setter method of dynamicContactModel - {@link T}
     * @param dynamicContactModel
     * @param value
     * @param setterName
     */
    private void setValueOfDynamicObject(T dynamicContactModel, Object value, String setterName) {

        if(dynamicContactModel != null){
            Class aClass = dynamicContactModel.getClass();
            Class[] paramTypes = new Class[1];
            if (setterName.equals(ModelSetterName.setDefaultNumberAndType.toString())){
                paramTypes[0] = PhoneNumberAndType.class;
            }else if (setterName.equals(ModelSetterName.setNumberAndType.toString()) || setterName.equals(ModelSetterName.setEmailAndType.toString()) || setterName.equals(ModelSetterName.setPostalAddressAndType.toString())){
                paramTypes[0] = ArrayList.class;
            }else if (setterName.equals(ModelSetterName.setImageUri.toString())){
                paramTypes[0] = Uri.class;
            }
            else{
                paramTypes[0] = String.class;
            }

            String methodName = setterName;
            Method m = null;
            try {
                m = aClass.getMethod(methodName, paramTypes);
            } catch (NoSuchMethodException nsme) {
                //nsme.printStackTrace();
            }

            try {
                if(m != null){
                    m.invoke(dynamicContactModel, value);
                }
            } catch (IllegalAccessException iae) {
                //iae.printStackTrace();
            } catch (InvocationTargetException ite) {
                //ite.printStackTrace();
            }
        }
    }

    /**
     * Responsible to create new instance of dynamicContactModel - {@link T}
     * @param dynamicContactModel
     * @return newInstance - {@link Object}
     */
    private Object getNewInstance(T dynamicContactModel) {
        Class c = null;
        Object newInstance = null;
        try {
            c = Class.forName(getClassName(dynamicContactModel));
        } catch (ClassNotFoundException e) {
            //e.printStackTrace();
        }
        if (c != null) {
            try {
                newInstance = c.newInstance();
            } catch (InstantiationException e) {
                //e.printStackTrace();
            } catch (IllegalAccessException e) {
                //e.printStackTrace();
            }
        }
        return newInstance;
    }

    /**
     * Responsible to get Class name dynamicContactModel {@link T}
     * @param dynamicContactModel
     * @return class name - {@link String}
     */
    protected String getClassName(T dynamicContactModel) {
        return dynamicContactModel.getClass().getName();
    }

    /**
     * Responsible to check dynamicContactModel - {@link T} is implemented Comparable
     * @param dynamicContactModel
     * @return {@link Boolean}
     */
    private boolean isComparableImplement(T dynamicContactModel){
        if(dynamicContactModel instanceof Comparable){
            return true;
        }
        return false;
    }
}