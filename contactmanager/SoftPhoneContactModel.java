/*
 * Project Name: libsbsipsdk [SB SIP PHONE SDK]
 * Developed By: Brotecs Technologies Limited
 * File Name: GenericContactModel.java
 * Created By: Mohammded Yeasin Arafat
 * Created Date: 1/19/16, 1:14 PM
 * Updated By: Mohammded Yeasin Arafat
 * Updated Date:  1/19/16
 */
package com.sb.sdk.contactmanager;

import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>SoftPhoneContactModel</h1>
 * <p>
 * SoftPhoneContactModel default SoftPhoneContact Model class
 * </p>
 *
 * @author Brotecs Technologies Limited
 * @version 1.0.1-26012016
 * @since 1/19/16
 */
public class SoftPhoneContactModel implements ISoftPhoneContactModel,Comparable<SoftPhoneContactModel> {
    private String id;
    private String displayName;
    private Uri imageUri;
    private PhoneNumberAndType defaultNumberAndType;
    private List<PhoneNumberAndType> numberAndType;
    private List<EmailAndType> emailAddressAndType;
    private List<PostalAddressAndType> postalAddressAndType;

    /**
     * Responsible to get Contact ID
     * @return id - {@link String}
     */
    public String getId() {
        return id;
    }

    @Override
    final public void setId(String id) {
        this.id = id;
    }

    /**
     * Responsible to get Display Name
     * @return displayName - {@link String}
     */
    public String getDisplayName() {
        return displayName;
    }

    @Override
    final public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Responsible to get Image Uri
     * @return imageUri - {@link String}
     */
    public Uri getImageUri() {
        return imageUri;
    }

    @Override
    final public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    /**
     * Responsible to return {@link PhoneNumberAndType} - Default Phone Number And Type
     * @return defaultNumberAndType - {@link PhoneNumberAndType}
     */
    public PhoneNumberAndType getDefaultNumberAndType() {
        return defaultNumberAndType;
    }

    @Override
    final public void setDefaultNumberAndType(PhoneNumberAndType defaultNumberAndType) {
        this.defaultNumberAndType = defaultNumberAndType;
    }

    /**
     * Responsible to return {@link List <PhoneNumberAndType>} - list of Phone Number And Type
     * @return {@link PhoneNumberAndType} numberAndType - list of Phone Number And Type
     */
    public List<PhoneNumberAndType> getNumberAndType() {
        return numberAndType;
    }

    @Override
    final public void setNumberAndType(ArrayList<PhoneNumberAndType> numberAndType) {
        this.numberAndType = numberAndType;
    }

    /**
     * Responsible to return {@link List <EmailAndType>} - list of Email And Type
     * @return {@link List <EmailAndType>} emailAddressAndType - list of Email And Type
     */
    public List<EmailAndType> getEmailAddressAndType() {
        return emailAddressAndType;
    }

    @Override
    final public void setEmailAndType(ArrayList<EmailAndType> emailAddressAndType) {
        this.emailAddressAndType = emailAddressAndType;
    }

    /**
     * Responsible to return {@link List <PostalAddressAndType>} - list of Postal Address And Type
     * @return {@link List <PostalAddressAndType>}  postalAddressAndType - list of Postal Address And Type
     */
    public List<PostalAddressAndType> getPostalAddressAndType() {
        return postalAddressAndType;
    }

    @Override
    final public void setPostalAddressAndType(ArrayList<PostalAddressAndType> postalAddressAndType) {
        this.postalAddressAndType = postalAddressAndType;
    }

    @Override
    public String toString() {
        return "SoftPhoneContactModel{" +
                "id='" + id + '\'' +
                ", displayName='" + displayName + '\'' +
                ", imageUri='" + imageUri + '\'' +
                ", defaultNumberAndType=" + defaultNumberAndType +
                ", numberAndType=" + numberAndType +
                ", emailAddressAndType=" + emailAddressAndType +
                ", postalAddressAndType=" + postalAddressAndType +
                '}';
    }
    @Override
    public int compareTo(SoftPhoneContactModel another) {
        return this.getDisplayName().compareToIgnoreCase(another.getDisplayName());
    }
}
