/*
 * Project Name: libsbsipsdk [SB SIP PHONE SDK]
 * Developed By: Brotecs Technologies Limited
 * File Name: ValidModelException.java
 * Created By: Mohammded Yeasin Arafat
 * Created Date: 1/19/16, 1:15 PM
 * Updated By: Mohammded Yeasin Arafat
 * Updated Date:  1/19/16
 */
package com.sb.sdk.contactmanager;

/**
 * <h1>ValidModelException</h1>
 * <p>
 * ValidModelException represents Model validation Exception
 * </p>
 *
 * @author Brotecs Technologies Limited
 * @version 1.0.1-26012016
 * @since 1/19/16
 */
public class ValidModelException extends Exception {
    /**
     * "className" represents Exception occured class Name
     */
    String className;
    /**
     * "exceptionMessages" represents Exception reasons/messages
     */
    String exceptionMessages;

    /**
     * Responsible to set Exception Messages and Exception occured class
     * @param exceptionMessages
     * @param className
     */
    ValidModelException(String exceptionMessages,String className){
        this.className = className;
        this.exceptionMessages = exceptionMessages;
    }

    @Override
    public String getMessage() {
        return "ValidModel Exception[ Invalid Model/Bean class : " + className + " Instruction(s) : "+exceptionMessages+"]";
    }

    @Override
    public String toString() {
        return "ValidModelException[ Invalid Model/Bean class : " + className + " Instruction(s) : "+exceptionMessages+"]";
    }
}