/*
 * Project Name: libsbsipsdk [SB SIP PHONE SDK]
 * Developed By: Brotecs Technologies Limited
 * File Name: DataLoaderNotification.java
 * Created By: Mohammded Yeasin Arafat
 * Created Date: 1/19/16, 2:07 PM
 * Updated By: Mohammded Yeasin Arafat
 * Updated Date:  1/19/16
 */
package com.sb.sdk.contactmanager;

/**
 * <h1>IDataLoaderNotification</h1>
 * <p>
 * IDataLoaderNotification should parent class of any Observe class which get notification
 * of contact's data load and Contact Model {@link T}
 * </p>
 *
 * @author Brotecs Technologies Limited
 * @version 1.0.1-26012016
 * @since 1/19/16
 */
public interface IDataLoaderNotification<T extends ISoftPhoneContact> {
    /**
     * Responsible to set dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact type
     * and "isLoadDone" Data loading state
     * @param isLoadDone
     * @param dynamicContactModel {@link T}
     */
    public void dataLoadConfirmation(boolean isLoadDone, T dynamicContactModel);

    /**
     * Responsible to set error message and handle Data loading after occured any error
     * @param error
     */
    public void errorMessage(String error);
}
