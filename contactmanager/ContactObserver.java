package com.sb.sdk.contactmanager;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import com.sb.sdk.applicationutilitymanager.CommonMethods;
import com.sb.sdk.base.SbSIPSDKCore;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Arafat on 8/31/2015.
 */
public class ContactObserver extends ContentObserver {
    ChangedContacts changedContacts;
    private static List<ContactChangeListener> changeListener;


    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);
        changedContacts.fireContactChangeEvent();
    }

    public static void setContactFragment(ContactChangeListener contactChangeListener) {
        if (changeListener == null) {
            changeListener = new ArrayList<>();
        }
        if (changeListener.size() > 0) {
            if (!changeListener.contains(contactChangeListener)) {
                changeListener.add(contactChangeListener);
            }
        } else {
            changeListener.add(contactChangeListener);
        }

    }

    public ContactObserver(Handler handler) {
        super(handler);
        changedContacts = new ChangedContacts(contactChangedListener);
        /**
         * fire event for first time to sync with recent db if any changed
         * this method is called from service for first time
         */
        changedContacts.fireContactChangeEvent();
    }

    @Override
    public boolean deliverSelfNotifications() {
        return super.deliverSelfNotifications();
    }

    private void notifyObserver(List<Integer> idList, String notifyType){
        if(notifyType.equals("delete")){
            new AsyncTaskForSynchronization().execute(idList);
        }else if (idList != null && idList.size() > 0) {
            new AsyncTaskForSynchronization().execute(idList);
        }
        if (changeListener != null) {
            for (ContactChangeListener contactChangeListener : changeListener) {
                contactChangeListener.contactChange(idList);
            }
        }
    }
    protected ChangedContacts.ContactChangedListener contactChangedListener = new ChangedContacts.ContactChangedListener() {
        @Override
        public void onChange(List<Integer> idList) {
            notifyObserver(idList,"update");
        }
        @Override
        public void onDelete(List<Integer> idList) {
            notifyObserver(idList,"delete");
        }
    };

    /**
     * Method to UPDATE Recent data record upon Contact Synchronization
     * Contact Reference Update
     * The Method also Refresh the Recent List Adapter
     */
    private synchronized void compileSynchronizationNumberList() {
        /*Fetch Phone List of the Recent DB*/
        List<String> recent_number_list = SbSIPSDKCore.getDatabaseObjectInitiator().RecentDAOManager.getNumberList();
        for (String temp : recent_number_list) {
            // TODO: Update Recent Database
            temp = CommonMethods.trimingString(temp);
            String recent_contact_reference = SbSIPSDKCore.getContactInstance().getContactIDByPhoneNumber(temp);
            SbSIPSDKCore.getDatabaseObjectInitiator().RecentDAOManager.synUpdateFromContact(temp, recent_contact_reference);
        }
    }

    /**
     * Method to UPDATE Recent data record upon Contact Synchronization
     * through Asynchronization Process
     */
    private class AsyncTaskForSynchronization extends AsyncTask<List<Integer>, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(List<Integer>... params) {
            compileSynchronizationNumberList();
            return null;
        }
    }
}
