/*
 * Project Name: libsbsipsdk [SB SIP PHONE SDK]
 * Developed By: Brotecs Technologies Limited
 * File Name: PhoneNumberAndType.java
 * Created By: Mohammded Yeasin Arafat
 * Created Date: 1/19/16, 1:25 PM
 * Updated By: Mohammded Yeasin Arafat
 * Updated Date:  1/19/16
 */
package com.sb.sdk.contactmanager;

/**
 * <h1>PhoneNumberAndType</h1>
 * <p>
 * PhoneNumberAndType is model class which represent of contact's single Phone Number and Type
 * </p>
 *
 * @author Brotecs Technologies Limited
 * @version 1.0.1-26012016
 * @since 1/19/16
 */
public class PhoneNumberAndType {
    /**
     * "phoneNumber" represents Single Phone number
     */
    private String phoneNumber;
    /**
     * "phoneNumberType" represents Single Phone number's type
     */
    private String phoneNumberType;

    /**
     *
     * @param phoneNumber
     * @param phoneNumberType
     */
    public PhoneNumberAndType(String phoneNumber,String phoneNumberType){
        this.phoneNumber = phoneNumber;
        this.phoneNumberType = phoneNumberType;
    }

    /**
     * Responsible to return single Phone Number
     * @return {@link String} phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Responsible to return single Phone Number Type
     * @return {@link String} phoneNumberType
     */
    public String getPhoneNumberType() {
        return phoneNumberType;
    }

    @Override
    public String toString() {
        return "PhoneNumberAndType{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", phoneNumberType='" + phoneNumberType + '\'' +
                '}';
    }
}