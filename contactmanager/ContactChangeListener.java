package com.sb.sdk.contactmanager;

import java.util.List;

/**
 * Created by arafat on 11/29/15.
 */
public interface ContactChangeListener {
    public void contactChange(List<Integer> idList);
}
