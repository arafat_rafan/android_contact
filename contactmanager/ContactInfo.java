/*
 * Project Name: libsbsipsdk [SB SIP PHONE SDK]
 * Developed By: Brotecs Technologies Limited
 * File Name: ContactInfo.java
 * Created By: Mohammded Yeasin Arafat
 * Created Date: 1/19/16, 1:13 PM
 * Updated By: Mohammded Yeasin Arafat
 * Updated Date:  1/19/16
 */
package com.sb.sdk.contactmanager;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>ContactInfo</h1>
 * <p>
 * ContactInfo is responsible to provide Contact's Data Set as Custom Model/Bean which must ISoftPhoneContact class or child class of ISoftPhoneContact type
 * It also possible to use without Custom Model/Bean and then ContactInfo will provide it's own model which is SoftPhoneContactModel
 * </p>
 *
 * @param <T> {@link T} - Custom Model/Bean and must ISoftPhoneContact class or child class of ISoftPhoneContact type
 * @author Brotecs Technologies Limited
 * @version 1.0.1-26012016-
 * @since 1/19/16
 */
public class ContactInfo<T extends ISoftPhoneContact> {
    /**
     * "contentResolver" instance of {@link ContentResolver}
     */
    private ContentResolver contentResolver;
    /**
     * "context" instance of {@link Context}
     */
    private Context context;
    /**
     * "dynamicContactModel" instance of T which is Custom Model/Bean and must ISoftPhoneContact class or child class of ISoftPhoneContact type
     */
    private T dynamicContactModel;
    /**
     * "observer" instance of IDataLoaderNotification type which responsible to notify data load state
     *  and send data to observed class
     */
    private IDataLoaderNotification<T> observer;
    /**
     * "willDataLoadInBackground" represents Contact Data will load background or not
     */
    private boolean willDataLoadInBackground;
    /**
     * "allAvailableSetterMethod" represents All available Setter Method in Contact API
     */
    private ArrayList<String> allAvailableSetterMethod;
    /**
     * "modelValidation" instance of {@link com.sb.sdk.contactmanager.ContactInfo.ModelValidation}
     * which is responsible to validate dynamicContactModel {@link T}
     */
    private ModelValidation modelValidation;
    /**
     * "contactDataManager" instance {@link ContactDataManager} which is responsible to fetch Contact Data from
     * Native/Phone contact
     */
    private ContactDataManager<T> contactDataManager;

    /**
     * Responsible to set ContentResolver and Context
     * @param context {@link Context} - Context can not be null; It's required Application Context {@link android.app.Application}
     * @throws NullPointerException
     */
    public ContactInfo(Context context) throws NullPointerException {

        if (context == null) {
            throw new NullPointerException("Context can not be null");
        }
        setContentResolver(context.getContentResolver());
        setContext(context);
        setAllAvailableSetterMethod();
        setModelValidation();
    }
    /**
     * Responsible to set ContentResolver and Context
     * @param contentResolver {@link ContentResolver} - ContentResolver an not be null
     * @param context {@link Context} - Context can not be null; It's required Application Context {@link android.app.Application}
     * @throws NullPointerException
     */
    public ContactInfo(ContentResolver contentResolver, Context context) throws NullPointerException {

        if (contentResolver == null) {
            throw new NullPointerException("ContentResolver can not be null");
        }
        if (context == null) {
            throw new NullPointerException("Context can not be null");
        }
        setContentResolver(contentResolver);
        setContext(context);
        setAllAvailableSetterMethod();
        setModelValidation();
    }

    /**
     * Responsible to set ContentResolver, ISoftPhoneContact type and Context
     * @param contentResolver {@link ContentResolver} - ContentResolver an not be null
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact type If only implement
     *                            ISoftPhoneContact then it should declared at least one setter method as defined in
     *                            com.sb.sdk.contactmanager.ModelSetterName {@link ModelSetterName}
     * @param context {@link Context} Context can not be null; It's required Application Context {@link android.app.Application}
     * @throws NullPointerException
     */
    private ContactInfo(ContentResolver contentResolver, T dynamicContactModel, Context context) throws NullPointerException {
        if (contentResolver == null) {
            throw new NullPointerException("ContentResolver can not be null");
        }
        if (context == null) {
            throw new NullPointerException("Context can not be null");
        }
        setContentResolver(contentResolver);
        setContext(context);
        setSoftPhoneContactModel(dynamicContactModel);
        setAllAvailableSetterMethod();
        setModelValidation();
    }

    /**
     * Responsible to set ContentResolver, willDataLoadInBackground, IDataLoaderNotification type, ISoftPhoneContact type and Context
     * @param contentResolver {@link ContentResolver} - ContentResolver can not be null
     * @param willDataLoadInBackground - if true then data will load in background and after finish load it will notify observed class
     * @param observer {@link IDataLoaderNotification<T>} - IDataLoaderNotification type to get notification of data load and data set
     * @param context {@link Context} - Context can not be null; It's required Application Context {@link android.app.Application}
     * @throws NullPointerException
     */
    private ContactInfo(ContentResolver contentResolver, boolean willDataLoadInBackground, IDataLoaderNotification<T> observer, Context context) throws NullPointerException {
        if (contentResolver == null) {
            throw new NullPointerException("ContentResolver can not be null");
        }
        if (context == null) {
            throw new NullPointerException("Context can not be null");
        }
        if (observer == null) {
            throw new NullPointerException("Observer can not be null");
        }
        setContentResolver(contentResolver);
        setContext(context);
        setDataLoadInBackground(willDataLoadInBackground);
        setDataLoadNotificationObserver(observer);
        setAllAvailableSetterMethod();
        setModelValidation(false);
    }

    /**
     * Responsible to set ContentResolver, willDataLoadInBackground, IDataLoaderNotification type,  ISoftPhoneContact type and Context
     * @param contentResolver {@link ContentResolver} - ContentResolver can not be null
     * @param willDataLoadInBackground - if true then data will load in background and after finish load it will notify observed class
     * @param dynamicContactModel - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared at least one setter method as defined in
     *                            com.sb.sdk.contactmanager.ModelSetterName {@link ModelSetterName}
     * @param observer {@link IDataLoaderNotification<T> } - IDataLoaderNotification type to get notification of data load and data set
     * @param context {@link Context} - Context can not be null; It's required Application Context {@link android.app.Application}
     * @throws NullPointerException
     */
    private ContactInfo(ContentResolver contentResolver, boolean willDataLoadInBackground, T dynamicContactModel, IDataLoaderNotification<T> observer, Context context) throws NullPointerException {
        if (contentResolver == null) {
            throw new NullPointerException("ContentResolver can not be null");
        }
        if (context == null) {
            throw new NullPointerException("Context can not be null");
        }
        if (observer == null) {
            throw new NullPointerException("Observer can not be null");
        }
        setContentResolver(contentResolver);
        setContext(context);
        setSoftPhoneContactModel(dynamicContactModel);
        setDataLoadInBackground(willDataLoadInBackground);
        setDataLoadNotificationObserver(observer);
        setAllAvailableSetterMethod();
        setModelValidation(false);
    }

    /**
     * Responsible to All available setter method
     * It's only available to ContactInfo
     */
    private void setAllAvailableSetterMethod() {
        allAvailableSetterMethod = new ArrayList<>();
        for (ModelSetterName modelSetterName : ModelSetterName.values()) {
            allAvailableSetterMethod.add(modelSetterName.toString());
        }
    }

    /**
     * Responsible to initialize ModelValidation class
     * It's only available to ContactInfo
     * @param isToCheckAllSetter - if true then add all available all setter method in supportedSetterMethod
     *                           to verify later all valid setter exist or not
     */
    private void setModelValidation(boolean isToCheckAllSetter) {
        modelValidation = new ModelValidation(isToCheckAllSetter);
    }
    /**
     * Responsible to initialize ModelValidation class
     * It's only available to ContactInfo
     */
    private void setModelValidation() {
        modelValidation = new ModelValidation();
    }

    /**
     * Responsible to initialize ContactDataManager class
     * It's only available to ContactInfo
     * @param contentResolver {@link ContentResolver}
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared at least one setter method as defined in
     *                            com.sb.sdk.contactmanager.ModelSetterName {@link ModelSetterName}
     * @param context {@link Context}
     */
    private void initializeContactDataManger(ContentResolver contentResolver, T dynamicContactModel, Context context) {
        /*if (contactDataManager == null) {
            setContentResolver(contentResolver);
            setSoftPhoneContactModel(dynamicContactModel);
            setContext(context);
            contactDataManager = new ContactDataManager<>(this.contentResolver, this.dynamicContactModel, this.context);
        }else{
            boolean willReinitializeContactDataManager = false;
            if(this.contentResolver == null || this.contentResolver != contentResolver){
                setContentResolver(contentResolver);
                willReinitializeContactDataManager = true;
            }
            if(this.dynamicContactModel == null || this.dynamicContactModel != dynamicContactModel){
                setSoftPhoneContactModel(dynamicContactModel);
                willReinitializeContactDataManager = true;
            }
            if(this.context == null || this.context != context){
                setContext(context);
                willReinitializeContactDataManager = true;
            }
            if(willReinitializeContactDataManager){
                contactDataManager = new ContactDataManager<>(this.contentResolver, this.dynamicContactModel, this.context);
            }
        }*/
        contactDataManager = new ContactDataManager<>(contentResolver, dynamicContactModel, context);
    }

    /**
     * * Responsible to initialize SoftPhoneContact Model class
     * It's only available to ContactInfo
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared at least one setter method as defined in
     *                            com.sb.sdk.contactmanager.ModelSetterName {@link ModelSetterName}
     */
    private void setSoftPhoneContactModel(T dynamicContactModel) {
        if(this.dynamicContactModel == null || this.dynamicContactModel != dynamicContactModel){
            this.dynamicContactModel = dynamicContactModel;
        }
    }

    /**
     * Responsible to set ContentResolver - {@link ContentResolver}
     * @param contentResolver
     */
    private void setContentResolver(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    /**
     * Responsible to set Context - {@link Context}
     * @param context
     */
    private void setContext(Context context) {
        this.context = context;
    }

    /**
     * Responsible to set Flag that data set load in background or not
     * @param willDataLoadInBackground
     */
    private void setDataLoadInBackground(boolean willDataLoadInBackground){
        this.willDataLoadInBackground = willDataLoadInBackground;
    }

    /**
     * Responsible to set Observer to send Data set and load notification
     * @param observer
     */
    private void setDataLoadNotificationObserver(IDataLoaderNotification<T> observer){
        this.observer = observer;
    }
    /**
     * Responsible to generate all available contact's Data set as Custom Model/Bean which must ISoftPhoneContact class
     * or child class of ISoftPhoneContact of as well as consist of DisplayName, Contact ID Default Number And Type And Image Uri
     * @param filterText - If null/Empty String then fetch all contact otherwise fetch only those contact where match with any data of contact
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared at least one setter method as defined in
     *                            com.sb.sdk.contactmanager.ModelSetterName {@link ModelSetterName}
     * @return List <{@link T}> List of dynamicContactModel
     * @throws ValidModelException
     * @throws NullPointerException
     */
    public List<T> getContactInfo(T dynamicContactModel,String filterText) throws ValidModelException, NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        if (modelValidation.isModelOnlyImplementISoftPhoneContact(this.dynamicContactModel)) {
            getSetterList(dynamicContactModel);
        }
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        return contactDataManager.getContactDisplayNameIDImageUri(getSetterList(dynamicContactModel), filterText);
    }

    /**
     * Responsible to generate all available contact's Data set as SoftPhoneContactModel
     * @param filterText - If null/Empty String then fetch all contact otherwise fetch only those contact where match with any data of contact
     * @return List <{@link SoftPhoneContactModel}> List of SoftPhoneContactModel
     */
    public List<T> getContactInfo(String filterText) {
        setSoftPhoneContactModel((T) new SoftPhoneContactModel());
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        return contactDataManager.getContactDisplayNameIDImageUri(null, filterText);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of DisplayName, Contact ID Default Number And Type And Image Uri
     * @param id Contact ID
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared at least one setter method as defined in
     *                            com.sb.sdk.contactmanager.ModelSetterName {@link ModelSetterName}
     * @return dynamicContactModel - {@link T}
     * @throws ValidModelException
     * @throws NullPointerException
     */
    public T getSingleContactInfoByID(String id, T dynamicContactModel) throws ValidModelException, NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        if (modelValidation.isModelOnlyImplementISoftPhoneContact(this.dynamicContactModel)) {
            getSetterList(dynamicContactModel);
        }
        return getSingleContactDisplayNameDefaultNumberAndTypeImageUriByID(id, this.dynamicContactModel);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of DisplayName, Contact ID Default Number And Type And Image Uri
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared at least one setter method as defined in
     *                            com.sb.sdk.contactmanager.ModelSetterName {@link ModelSetterName}
     * @return dynamicContactModel - {@link T}
     * @throws ValidModelException
     * @throws NullPointerException
     */
    public T getSingleContactInfoByPhoneNumber(String number, T dynamicContactModel) throws ValidModelException, NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        return getSingleContactInfoByID(contactDataManager.getContactID(number), this.dynamicContactModel);
    }

    /**
     * Responsible to generate contact's Data set of SoftPhoneContactModel consist of DisplayName, Contact ID Default Number And Type And Image Uri
     * @param id Contact ID
     * @return {@link SoftPhoneContactModel} - SoftPhoneContactModel
     */
    public SoftPhoneContactModel getSingleContactInfoByID(String id) {
        setSoftPhoneContactModel((T) new SoftPhoneContactModel());
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        if(id == null){
            return null;
        }
        return (SoftPhoneContactModel) contactDataManager.getSingleContactDisplayNameDefaultNumberAndTypeImageUriByID(id, null);
    }

    /**
     * Responsible to generate contact's Data set of SoftPhoneContactModel consist of DisplayName, Contact ID Default Number And Type And Image Uri
     * @param number Phone Number
     * @return {@link SoftPhoneContactModel} - SoftPhoneContactModel
     */
    public SoftPhoneContactModel getSingleContactInfoByPhone(String number) {
        setSoftPhoneContactModel((T) new SoftPhoneContactModel());
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        return (SoftPhoneContactModel)getSingleContactInfoByID(contactDataManager.getContactID(number));
    }
    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Display Name, Image Uri
     * @param id Contact ID
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setDisplayName" , "setDefaultNumberAndType" and "setImageUri" setter methods
     * @return dynamicContactModel - {@link T}
     * @throws ValidModelException
     * @throws NullPointerException
     */
    private T getSingleContactDisplayNameImageUriByID(String id, T dynamicContactModel) throws ValidModelException, NullPointerException {
        if(id != null){
            if (dynamicContactModel == null) {
                throw new NullPointerException(nullPointerExceptionMessage());
            }
            setSoftPhoneContactModel(dynamicContactModel);
            if (modelValidation.isModelOnlyImplementISoftPhoneContact(this.dynamicContactModel)) {
                return contactDataManager.getSingleContactDisplayNameImageUriByID(id, getSetterList(this.dynamicContactModel));
            } else {
                return contactDataManager.getSingleContactDisplayNameImageUriByID(id, new ArrayList<String>());
            }
        }else{
            return null;
        }
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of DisplayName, Contact ID Default Number And Type, Image Uri,
     * List of Number And Type list, List of Email And Type list as well as List of PostalAddress and Type
     * @param id Contact ID
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared at least one setter method as defined in
     *                            com.sb.sdk.contactmanager.ModelSetterName {@link ModelSetterName}
     * @return dynamicContactModel - {@link T}
     * @throws ValidModelException
     * @throws NullPointerException
     */
    public T getContactDetailsByID(String id, T dynamicContactModel) throws ValidModelException, NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        if (modelValidation.isModelOnlyImplementISoftPhoneContact(dynamicContactModel)) {
            getSetterList(dynamicContactModel);
        }
        getSingleContactDisplayNameImageUriByID(id, this.dynamicContactModel);
        getContactAddressAndTypeListByID(id, this.dynamicContactModel);
        getContactNumberAndTypeListByID(id, this.dynamicContactModel);
        getContactEmailAndTypeListByID(id, this.dynamicContactModel);
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of SoftPhoneContactModel consist of DisplayName, Contact ID Default Number And Type, Image Uri,
     * List of Number And Type list, List of Email And Type list as well as List of PostalAddress and Type
     * @param id Contact ID
     * @return {@link SoftPhoneContactModel} - SoftPhoneContactModel
     */
    public SoftPhoneContactModel getContactDetailsByID(String id) {
        if(id != null){
            setSoftPhoneContactModel((T) new SoftPhoneContactModel());
            initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
            contactDataManager.getSingleContactDisplayNameImageUriByID(id, null);
            contactDataManager.getContactAddressAndTypeListByID(id);
            contactDataManager.getContactNumberAndTypeListByID(id);
            return (SoftPhoneContactModel)contactDataManager.getContactEmailAndTypeListByID(id);
        }else{
            return null;
        }
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of DisplayName, Contact ID Default Number And Type, Image Uri,
     * List of Number And Type list, List of Email And Type list as well as List of PostalAddress and Type
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared at least one setter method as defined in
     *                            com.sb.sdk.contactmanager.ModelSetterName {@link ModelSetterName}
     * @return dynamicContactModel - {@link T}
     * @throws ValidModelException
     * @throws NullPointerException
     */
    public T getContactDetailsByPhoneNumber(String number, T dynamicContactModel) throws ValidModelException, NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        return getContactDetailsByID(contactDataManager.getContactID(number), this.dynamicContactModel);
    }

    /**
     * Responsible to generate contact's Data set of SoftPhoneContactModel consist of DisplayName, Contact ID Default Number And Type, Image Uri,
     * List of Number And Type list, List of Email And Type list as well as List of PostalAddress and Type
     * @param number Phone Number
     * @return {@link SoftPhoneContactModel} - SoftPhoneContactModel
     */
    public SoftPhoneContactModel getContactDetailsByPhoneNumber(String number) {
        setSoftPhoneContactModel((T) new SoftPhoneContactModel());
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        return (SoftPhoneContactModel)getContactDetailsByID(contactDataManager.getContactID(number));
    }

    /**
     * Responsible to generate Contact ID from a given Phone Number if found otherwise return null
     * @param number Phone Number
     * @return {@link String} Contact ID
     */
    public String getContactIDByPhoneNumber(String number) {
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        if(number != null && number.length() > 0){
            return contactDataManager.getContactID(number);
        }else{
            return null;
        }
    }

    /**
     * Responsible to generate Contact ID from a given Phone Number if found otherwise return null
     * @param number Phone Number
     * @return {@link String} Contact ID
     */
    public long getContactIdByExtension(String number) {
        long contactId = 0L;
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        if(number != null && number.length() > 0){
            String contactIdStr = contactDataManager.getContactID(number);
            if (contactIdStr != null){
                try {
                    contactId = Long.parseLong(contactIdStr);
                }catch (NumberFormatException e){
                    contactId = 0L;
                }
            }
        }

        return contactId;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Contact ID
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setId" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactIDByPhoneNumber(String number, T dynamicContactModel) throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        getContactIDByPhoneNumber(number);
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Image Uri
     * dynamicContactModel must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     * ISoftPhoneContact then it should declared "setImageUri" setter method
     * @param id Contact ID
     * @return dynamicContactModel - {@link T}
     */
    private T getContactImageUriByID(String id) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Image Uri
     * @param id Contact ID
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setImageUri" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactImageUriByID(String id, T dynamicContactModel) throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactImageUriByID(id);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Image Uri
     * @param number Phone Number
     * @return dynamicContactModel - {@link T}
     */
    private T getContactImageUriByPhoneNumber(String number) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Image Uri
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setImageUri" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactImageUriByPhoneNumber(String number, T dynamicContactModel) throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactImageUriByPhoneNumber(number);
    }

    /**
     *Responsible to generate contact's Data set of dynamicContactModel consist of Display Name
     * @param id Contact ID
     * @return dynamicContactModel - {@link T}
     */
    private T getContactNameByID(String id) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Display Name
     * @param id
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setDisplayName" setter method
     * @return dynamicContactModel
     * @throws NullPointerException
     */
    private T getContactNameByID(String id, T dynamicContactModel) throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactNameByID(id);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Display Name
     * @param number Phone Number
     * @return dynamicContactModel - {@link T}
     */
    private T getContactNameByPhoneNumber(String number) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Display Name
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setDisplayName" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactNameByPhoneNumber(String number, T dynamicContactModel) throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactNameByPhoneNumber(number);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Number And Type as well as
     * Image Uri
     * @param id Contact ID
     * @return dynamicContactModel - {@link T}
     */
    private T getContactNameNumberAndTypeListImageUriByID(String id) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Number And Type as well as
     * Image Uri
     * @param id Contact ID
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setNumberAndType" and "setImageUri" setter methods
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactNameNumberAndTypeListImageUriByID(String id, T dynamicContactModel) throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactNameNumberAndTypeListImageUriByID(id);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Display Name, Default Number And Type as well as
     * Image Uri
     * @param id Contact ID
     * @return dynamicContactModel - {@link T}
     */
    private T getSingleContactDisplayNameDefaultNumberAndTypeImageUriByID(String id, ArrayList<String> setterList) {
        if (contactDataManager != null) {
            return (T) contactDataManager.getSingleContactDisplayNameDefaultNumberAndTypeImageUriByID(id, setterList);
        } else {
            initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
            return (T) contactDataManager.getSingleContactDisplayNameDefaultNumberAndTypeImageUriByID(id, setterList);
        }
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Display Name, Default Number And Type as well as
     * Image Uri
     * @param id Contact ID
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setDisplayName" , "setDefaultNumberAndType" and "setImageUri" setter methods
     * @return dynamicContactModel - {@link T}
     * @throws ValidModelException
     * @throws NullPointerException
     */
    private T getSingleContactDisplayNameDefaultNumberAndTypeImageUriByID(String id, T dynamicContactModel) throws ValidModelException, NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        if (modelValidation.isModelOnlyImplementISoftPhoneContact(this.dynamicContactModel)) {
            return getSingleContactDisplayNameDefaultNumberAndTypeImageUriByID(id, getSetterList(this.dynamicContactModel));
        } else {
            return getSingleContactDisplayNameDefaultNumberAndTypeImageUriByID(id, new ArrayList<String>());
        }
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Display Name, Default Number And Type as well as
     * Image Uri
     * @param number Phone Number
     * @return dynamicContactModel - {@link T}
     */
    private T getSingleContactDisplayNameDefaultNumberAndTypeImageUriByPhoneNumber(String number) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Display Name, Default Number And Type as well as
     * Image Uri
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setDisplayName" , "setDefaultNumberAndType" and "setImageUri" setter methods
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getSingleContactDisplayNameDefaultNumberAndTypeImageUriByPhoneNumber(String number, T dynamicContactModel)throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getSingleContactDisplayNameDefaultNumberAndTypeImageUriByPhoneNumber(number);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of ID, Display Name as well as
     * Image Uri
     * @param number Phone Number
     * @return dynamicContactModel - {@link T}
     */
    private T getContactIDNameImageUriByPhoneNumber(String number) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of ID, Display Name as well as
     * Image Uri
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setId" , "setDisplayName" and "setImageUri" setter methods
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactIDNameImageUriByPhoneNumber(String number, T dynamicContactModel)throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactIDNameImageUriByPhoneNumber(number);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Display Name as well as
     * Image Uri
     * @param number Phone Number
     * @return dynamicContactModel - {@link T}
     */
    private T getContactNameImageUriByPhoneNumber(String number) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Display Name as well as
     * Image Uri
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setDisplayName" and "setImageUri" setter methods
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactNameImageUriByPhoneNumber(String number, T dynamicContactModel) throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactNameImageUriByPhoneNumber(number);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Number And Type
     * @param id Contact ID
     * @return dynamicContactModel - {@link T}
     */
    private T getContactNumberAndTypeListByID(String id) {
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        return (T) contactDataManager.getContactNumberAndTypeListByID(id);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Number And Type
     * @param id Contact ID
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setNumberAndType" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactNumberAndTypeListByID(String id, T dynamicContactModel) throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        return getContactNumberAndTypeListByID(id);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Number And Type
     * @param number Phone Number
     * @return dynamicContactModel - {@link T}
     */
    private T getContactNumberAndTypeListByPhoneNumber(String number) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Number And Type
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setNumberAndType" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactNumberAndTypeListByPhoneNumber(String number, T dynamicContactModel)throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactNumberAndTypeListByPhoneNumber(number);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Default Number And Type
     * @param id Contact ID
     * @return dynamicContactModel - {@link T}
     */
    private T getContactDefaultNumberAndTypeByID(String id) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Default Number And Type
     * @param id Contact ID
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setDefaultNumberAndType" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactDefaultNumberAndTypeByID(String id, T dynamicContactModel)throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactDefaultNumberAndTypeByID(id);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Default Number And Type
     * @param number Phone Number
     * @return dynamicContactModel - {@link T}
     */
    private T getContactDefaultNumberAndTypeByPhoneNumber(String number) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of Default Number And Type
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setDefaultNumberAndType" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactDefaultNumberAndTypeByPhoneNumber(String number, T dynamicContactModel)throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactDefaultNumberAndTypeByPhoneNumber(number);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of ID, List of Number And Type
     * @param number Phone Number
     * @return dynamicContactModel - {@link T}
     */
    private T getContactIDtNumberAndTypeListByPhoneNumber(String number) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of ID, List of Number And Type
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setId", "setNumberAndType" setter methods
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactIDtNumberAndTypeListByPhoneNumber(String number, T dynamicContactModel)throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactIDtNumberAndTypeListByPhoneNumber(number);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Email And Type
     * @param id Contact ID
     * @return dynamicContactModel - {@link T}
     */
    private T getContactEmailAndTypeListByID(String id) {
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        return (T) contactDataManager.getContactEmailAndTypeListByID(id);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Email And Type
     * @param id Contact ID
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setEmailAndType" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactEmailAndTypeListByID(String id, T dynamicContactModel) throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        return getContactEmailAndTypeListByID(id);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Email And Type
     * @param number Phone Number
     * @return dynamicContactModel - {@link T}
     */
    private T getContactEmailAndTypeListByPhoneNumber(String number) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Email And Type
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setEmailAndType" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactEmailAndTypeListByPhoneNumber(String number, T dynamicContactModel)throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactEmailAndTypeListByPhoneNumber(number);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Postal Address And Type
     * @param id Contact ID
     * @return dynamicContactModel -{@link T}
     */
    private T getContactAddressAndTypeListByID(String id) {
        initializeContactDataManger(contentResolver, this.dynamicContactModel, context);
        return (T) contactDataManager.getContactAddressAndTypeListByID(id);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Postal Address And Type
     * @param id Contact ID
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setPostalAddressAndType" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactAddressAndTypeListByID(String id, T dynamicContactModel)throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        return getContactAddressAndTypeListByID(id);
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Postal Address And Type
     * @param number Phone Number
     * @return dynamicContactModel - {@link T}
     */
    private T getContactAddressAndTypeListByPhoneNumber(String number) {
        // TODO: Data Manipulation
        return this.dynamicContactModel;
    }

    /**
     * Responsible to generate contact's Data set of dynamicContactModel consist of List of Postal Address And Type
     * @param number Phone Number
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact If only implement
     *                            ISoftPhoneContact then it should declared "setPostalAddressAndType" setter method
     * @return dynamicContactModel - {@link T}
     * @throws NullPointerException
     */
    private T getContactAddressAndTypeListByPhoneNumber(String number, T dynamicContactModel) throws NullPointerException {
        if (dynamicContactModel == null) {
            throw new NullPointerException(nullPointerExceptionMessage());
        }
        setSoftPhoneContactModel(dynamicContactModel);
        // TODO: Data Manipulation
        return getContactAddressAndTypeListByPhoneNumber(number);
    }

    /**
     * Responsible to generate class name
     * @param dynamicContactModel must ISoftPhoneContact class or child class of ISoftPhoneContact
     * @return Class Name - {@link String}
     */
    protected String getClassName(T dynamicContactModel) {
        return dynamicContactModel.getClass().getName();
    }

    /**
     *
     * @return NullPointer Exception Message - {@link String}
     */
    private String nullPointerExceptionMessage() {
        return "Contact Model/Bean can not be null";
    }

    /**
     * Responsible to generate all available valid setter from dynamicContactModel
     * @param dynamicContactModel {@link T} - must ISoftPhoneContact class or child class of ISoftPhoneContact
     * @return List<{@link String}> List if setter method name
     * @throws ValidModelException
     */
    protected ArrayList<String> getSetterList(T dynamicContactModel) throws ValidModelException {
        ArrayList<String> setterMethodListInClass = null;
        Method[] methodName = dynamicContactModel.getClass().getMethods();
        if (methodName.length < 1) {
            throw new ValidModelException("No valid setter method found! At least one setter method require from this list -> \"" + java.util.Arrays.asList(ModelSetterName.values()) + "\"", getClassName(dynamicContactModel));
        } else {
            setterMethodListInClass = new ArrayList<>();
        }
        for (Method name : methodName
                ) {
            for (ModelSetterName modelSetterName : ModelSetterName.values()
                    ) {
                if (modelSetterName.toString().equals(name.getName())) {
                    setterMethodListInClass.add(modelSetterName.toString());
                    break;
                }
            }
        }
        if (setterMethodListInClass.size() < 1) {
            throw new ValidModelException("No valid setter method found! At least one setter method require from this list -> \"" + java.util.Arrays.asList(ModelSetterName.values()) + "\"", getClassName(dynamicContactModel));
        }
        return setterMethodListInClass;
    }

    /**
     * Convert Image Uri to Bitmap
     * @param imageUri
     * @return {@link Bitmap}
     * @throws IOException
     */
    public Bitmap imageUriToBitmap(Uri imageUri) throws IOException{
        if(imageUri != null){
            return MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri);
        }else{
         return null;
        }
    }
    /**
     * <h1>ModelValidation</h1>
     * <p>
     * ModelValidation is a final class . It responsible to verified the dynamic model is a valid ISoftPhoneContact class or child class of ISoftPhoneContact type
     * </p>
     *
     */
    final class ModelValidation {

        List<String> supportedSetterMethod;

        /**
         * Default Constructor
         */
        ModelValidation() {
        }

        /**
         *
         * @param isToCheckAllSetter if true then add all available all setter method in supportedSetterMethod
         *                           to verify later all valid setter exist or not
         */
        ModelValidation(boolean isToCheckAllSetter) {
            if (isToCheckAllSetter) {
                supportedSetterMethod = new ArrayList<>();
                for (ModelSetterName modelSetterName : ModelSetterName.values()
                        ) {
                    supportedSetterMethod.add(modelSetterName.toString());
                }
            }

        }

        /**
         * Responsible to check is dynamicContactModel only implement ISoftPhoneContact type or not
         * @param dynamicContactModel
         * @return {@link Boolean} - if only ISoftPhoneContact type then true
         */
        public boolean isModelOnlyImplementISoftPhoneContact(T dynamicContactModel) {
            if (dynamicContactModel instanceof SoftPhoneContactModel || dynamicContactModel instanceof ISoftPhoneContactModel) {
                return false;
            }
            return true;
        }

        /**
         * @param dynamicContactModel
         * @return {@link Boolean}
         */
        @Deprecated
        private boolean isModelExtend(T dynamicContactModel) {
            if (dynamicContactModel instanceof SoftPhoneContactModel) {
                return true;
            }
            return false;
        }

        /**
         * Responsible to check all possible valid setter method exist or not
         * @param dynamicContactModel - {@link T}
         * @return {@link Boolean} - if all valid setter exist then true
         */
        protected boolean isAllPossibleSetterMethodExist(T dynamicContactModel) throws ValidModelException {
            ArrayList<String> setterMethodListInClass = null;
            Method[] methodName = dynamicContactModel.getClass().getMethods();
            if (methodName.length < 1) {
                throw new ValidModelException("No valid setter method found! Must exist all of listed setter methods\"" + java.util.Arrays.asList(ModelSetterName.values()) + "\"", getClassName(dynamicContactModel));
            } else {
                setterMethodListInClass = new ArrayList<>();
            }
            for (Method name : methodName
                    ) {
                for (ModelSetterName modelSetterName : ModelSetterName.values()
                        ) {
                    if (modelSetterName.toString().equals(name.getName())) {
                        setterMethodListInClass.add(modelSetterName.toString());
                        break;
                    }
                }
            }
            if (setterMethodListInClass.size() == ModelSetterName.values().length) {
                return true;
            }
            for (ModelSetterName modelSetterName : ModelSetterName.values()
                    ) {
                if (!allAvailableSetterMethod.contains(modelSetterName.toString())) {
                    throw new ValidModelException(modelSetterName.toString() + " is not declared as expected! Must exist all of listed setter methods\"" + java.util.Arrays.asList(ModelSetterName.values()) + "\"", getClassName(dynamicContactModel));
                }
            }
            return false;
        }

        /**
         * Responsible to check is list of setter exist or not
         * @param dynamicContactModel
         * @return {@link Boolean} - if all setter exist of given list then return true
         */
        protected boolean isValidSetterMethodExist(T dynamicContactModel, ArrayList<String> setterMethodList) throws ValidModelException {
            ArrayList<String> setterMethodListInClass = null;
            Method[] methodName = dynamicContactModel.getClass().getMethods();
            if (methodName.length < 1) {
                throw new ValidModelException("No valid setter method found! Must exist all of listed setter methods\"" + setterMethodList.toString() + "\"", getClassName(dynamicContactModel));
            } else {
                setterMethodListInClass = new ArrayList<>();
            }
            for (Method name : methodName
                    ) {
                for (String setterName : setterMethodList
                        ) {
                    if (setterName.equals(name.getName())) {
                        setterMethodListInClass.add(setterName);
                        break;
                    }

                }

            }
            if (setterMethodList.size() == setterMethodListInClass.size()) {
                return true;
            }
            for (String name : setterMethodList
                    ) {
                if (!setterMethodListInClass.contains(name)) {
                    throw new ValidModelException(name + " is not declared as expected! Must exist all of listed setter methods\"" + setterMethodList.toString() + "\"", getClassName(dynamicContactModel));
                }
            }
            return false;
        }
    }
}