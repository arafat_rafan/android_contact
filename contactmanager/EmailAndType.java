/*
 * Project Name: libsbsipsdk [SB SIP PHONE SDK]
 * Developed By: Brotecs Technologies Limited
 * File Name: EmailAndType.java
 * Created By: Mohammded Yeasin Arafat
 * Created Date: 1/19/16, 1:23 PM
 * Updated By: Mohammded Yeasin Arafat
 * Updated Date:  1/19/16
 */
package com.sb.sdk.contactmanager;

/**
 * <h1>EmailAndType</h1>
 * <p>
 * EmailAndType is model class which represent of contact's single Email And Type
 * </p>
 *
 * @author Brotecs Technologies Limited
 * @version 1.0.1-26012016
 * @since 1/19/16
 */
public class EmailAndType {
    /**
     * "email" represents Single Email data
     */
    private String email;
    /**
     *  "emailType" represents Single Email data's type
     */
    private String emailType;

    /**
     * Responsible to set single email and type
     * @param email
     * @param emailType
     */
    public EmailAndType(String email, String emailType){
        this.email = email;
        this.emailType = emailType;
    }

    /**
     * Responsible to return single email
     * @return {@link String} - email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Responsible to return single email type
     * @return {@link String} - emailType
     */
    public String getEmailType() {
        return emailType;
    }

    @Override
    public String toString() {
        return "EmailAndType{" +
                "email='" + email + '\'' +
                ", emailType='" + emailType + '\'' +
                '}';
    }
}