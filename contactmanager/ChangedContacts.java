/**
 * Created by Arafat on 9/1/2015.
 */

package com.sb.sdk.contactmanager;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;

import com.sb.sdk.applicationutilitymanager.ApplicationSharedPreferences;
import com.sb.sdk.base.SbSIPSDKCore;

import java.util.ArrayList;
import java.util.List;

/**
 * Demonstrates selecting contacts that have changed since a certain time.
 */
public class ChangedContacts {

    private static final String CLASS = ChangedContacts.class.getSimpleName();


    private static final String PREF_KEY_CHANGE = "timestamp_change";
    private static final String PREF_KEY_DELETE = "timestamp_delete";

    private static final int ID_CHANGE_LOADER = 1;
    private static final int ID_DELETE_LOADER = 2;

    private long mSearchTime;

    private ContactChangedListener contactChangedListener;

    public interface ContactChangedListener {
        void onChange(List<Integer> idList);

        void onDelete(List<Integer> idList);
    }


    public ChangedContacts(ContactChangedListener contactChangedListener) {
        this.contactChangedListener = contactChangedListener;
    }

    public synchronized void fireContactChangeEvent(){
        AsyncQueryExecutor changeQueryExecutor = new AsyncQueryExecutor();

        AsyncQueryExecutor deleteQueryExecutor = new AsyncQueryExecutor();

        Integer[] params_change = new Integer[] {ID_CHANGE_LOADER};
        Integer[] params_delete = new Integer[] {ID_DELETE_LOADER};

        try {
            changeQueryExecutor.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, params_change);
            deleteQueryExecutor.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, params_delete);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }



    private class AsyncQueryExecutor extends AsyncTask<Integer, Void, List<Integer>> {
        int query_type = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<Integer> idList) {
            super.onPostExecute(idList);
            switch (query_type) {
                case ID_CHANGE_LOADER:
                    if(idList != null && idList.size() > 0) {
                        contactChangedListener.onChange(idList);
                    }
                    break;
                case ID_DELETE_LOADER:
                    contactChangedListener.onDelete(idList);
                    break;
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected List<Integer> doInBackground(Integer... params) {
            query_type = params[0];

            switch (query_type){
                case ID_CHANGE_LOADER:
                    return getChangedContactIdList();
                case ID_DELETE_LOADER:
                    return null;
            }
            return null;
        }
    }


    private void saveLastTimestamp(long time) {
       ApplicationSharedPreferences.setContactChangeLastTimeStamp(time);
    }

    private long getLastTimestamp(long time, String key) {
       return ApplicationSharedPreferences.getContactChangeLastTimeStamp();
    }


    private List<Integer> getChangedContactIdList() {

        long timestamp = 0;

        String[] projection = new String[]{
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.CONTACT_LAST_UPDATED_TIMESTAMP
        };

        mSearchTime = getLastTimestamp(0, PREF_KEY_CHANGE);

        String selection = ContactsContract.Data.CONTACT_LAST_UPDATED_TIMESTAMP + " > ?";
        String[] bindArgs = new String[]{mSearchTime + ""};


        Cursor cursor = SbSIPSDKCore.getContext().getContentResolver().query(ContactsContract.Data.CONTENT_URI, projection,
                selection, bindArgs, ContactsContract.Data.CONTACT_LAST_UPDATED_TIMESTAMP
                        + " desc, " + ContactsContract.Data.CONTACT_ID + " desc");

        if (cursor.moveToFirst()) {
            timestamp = cursor.getLong(cursor.getColumnIndex(
                    ContactsContract.Data.CONTACT_LAST_UPDATED_TIMESTAMP));
        }
        if (timestamp > 0) {
            saveLastTimestamp(timestamp);
        }

        return getDistinctContactIds(cursor);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private List<Integer> getDeletedContactIdList() {

        long timestamp = 0;

        String[] projection = new String[]{
                ContactsContract.DeletedContacts.CONTACT_ID,
                ContactsContract.DeletedContacts.CONTACT_DELETED_TIMESTAMP
        };

        mSearchTime = getLastTimestamp(0, PREF_KEY_DELETE);

        String selection = ContactsContract.DeletedContacts.CONTACT_DELETED_TIMESTAMP + " > ?";
        String[] bindArgs = new String[]{mSearchTime + ""};

        Cursor cursor =  SbSIPSDKCore.getContext().getContentResolver().query(ContactsContract.DeletedContacts.CONTENT_URI, projection,
                selection, bindArgs, ContactsContract.DeletedContacts.CONTACT_DELETED_TIMESTAMP +
                " desc");



        if (cursor.moveToFirst()) {
            timestamp = cursor.getLong(cursor.getColumnIndex(
                    ContactsContract.DeletedContacts.CONTACT_DELETED_TIMESTAMP));
        }

        if (timestamp > 0) {
            saveLastTimestamp(timestamp);
        }

        return getDistinctContactIds(cursor);

    }


    private List<Integer> getDistinctContactIds(Cursor cursor){
        List<Integer> idList = new ArrayList<Integer>();
        if(cursor.moveToFirst()){
            do{
                int id = cursor.getInt(0);
                boolean isExist = false;
                if(idList.size() > 0) {
                    for (Integer _id: idList){
                        if (_id == id) {
                            isExist = true;
                            break;
                        }
                    }

                }

                if(!isExist){
                    idList.add(id);
                }
            }while (cursor.moveToNext());
        }

        return idList;
    }


}
